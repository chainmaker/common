#!/usr/bin/env bash
#
# Copyright (C) THL A29 Limited, a Tencent company. All rights reserved.
#
# SPDX-License-Identifier: Apache-2.0
#
function install() {
  os_name=$(lsb_release -i | cut -f2)
  os_version=$(lsb_release -r | cut -f2)
  if [ "$os_name" == "Ubuntu" ] && [ "$os_version" == "18.04" ]; then
    sudo apt-get install -y softhsm2
  elif [ "$os_name" == "Ubuntu" ] && [ "$os_version" == "20.04" ]; then
    sudo apt-get install -y softhsm2
  elif [ "$os_name" == "CentOS" ] && [ "$os_version" == "7.9.2009" ]; then
    sudo yum install -y softhsm2
  elif [ "$os_name" == "CentOS" ] && [ "$os_version" == 8* ]; then
    sudo dnf install -y softhsm2
  else
    echo "Unsupported OS"
    exit 1
  fi
}

install
